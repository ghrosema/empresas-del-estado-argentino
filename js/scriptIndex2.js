let cardFinal = '';
$.getJSON("empresas.json", function(data) {

    poblarHeader(data);
    poblarSection(data);

});

function poblarHeader(empresasJSON) {
    $('#nombrePro').html(empresasJSON.trabajo);
    $('#descripcionPro').html(empresasJSON.descripcion);
}

function poblarSection(empresasJSON) {
    let card = '';
    let company = empresasJSON.empresas;
    for (var i = 0; i < company.length; i++) {

        card += '<div class="col-sm-6 col-md-4">';
        card += '<div class="thumbnail">';
        card += '<div class="caption">';
        card += '<h3>' + company[i].nombre + '</h3>';
        card += '<p><b> Fundación: </b>' + company[i].fundacion + '</p>';
        card += '<p><b> propiedadActual: </b>' + company[i].propiedadActual + '</p>';
        card += '<ul>';
        for (var j = 0; j < company[i].hitos.length; j++) {
            card += "<li>" + company[i].hitos[j] + "</li>";
        };

        card += '</ul>';
        //card += '<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>';
        card += '</div></div></div>';
    }
    $('#empresas').html(card);
    cardFinal = card;
}