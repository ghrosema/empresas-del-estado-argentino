//const header = document.querySelector('header');
const banner = document.querySelector('div.banner');
const content = document.querySelector('div.l-content');
const tablas = document.querySelector('div.pricing-tables');

const request = new XMLHttpRequest();
request.open('GET', 'empresas.json')
request.responseType = 'json';
request.send();

request.onload = function() {
    const empresas = request.response;
    poblarHeader(empresas);
    poblarSection(empresas);
}

function poblarHeader(empresasJSON) {
    //console.log(empresasJSON.trabajo);
    const miH1 = document.createElement('h1');
    miH1.className = 'banner-head';
    miH1.innerHTML = empresasJSON['trabajo'] + '<br>' + empresasJSON.descripcion;
    banner.appendChild(miH1);
}

function poblarSection(empresasJSON) {
    const empresas = empresasJSON['empresas'];
    //console.log(empresas.length);

    for (var i = 0; i < empresas.length; i++) {
        const miContenedor = document.createElement('div');
        miContenedor.className = "pure-u-1 pure-u-md-1-3";

        const miCaja = document.createElement('div');
        miCaja.className = "pricing-table pricing-table-free";

        const miH2 = document.createElement('h2');
        miH2.className = "pricing-table-header";

        const fundacion = document.createElement('div');

        const miLista = document.createElement('ul');
        miLista.className = "pricing-table-list"
        //console.log(empresas[i].hitos);
        for (var j = 0; j < empresas[i].hitos.length; j++) {
            const miItem = document.createElement('li');
            miItem.textContent = empresas[i].hitos[j];
            miLista.appendChild(miItem);
        }

        miH2.innerHTML = empresas[i].nombre + '<br>';
        fundacion.textContent = 'fundación: ' + empresas[i].fundacion;

        miH2.appendChild(fundacion);
        miCaja.appendChild(miH2);
        //myArticle.appendChild(myPara1);
        //myArticle.appendChild(myPara2);
        miCaja.appendChild(miLista);

        miContenedor.appendChild(miCaja);
        tablas.appendChild(miContenedor);
    }
}
